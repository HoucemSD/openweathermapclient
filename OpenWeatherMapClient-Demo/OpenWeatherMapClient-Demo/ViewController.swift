//
//  ViewController.swift
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
import OpenWeatherMapClient

class ViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    @objc func searchAction(){
        OWMClient.getCityWeather(searchTextField.text) { [weak self] (weather, error) in
            guard error == nil else {
                self?.resultLabel.text = error?.localizedDescription
                return
            }
            if let temperature = weather?.currentTemp?.intValue {
                self?.resultLabel.text = "\(temperature)°"
            }else {
                self?.resultLabel.text = "Result Format Error"
            }
        }
    }
    
    private func setupViews() {
        searchTextField.layer.cornerRadius = 10
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        searchTextField.leftView = leftPaddingView
        searchTextField.leftViewMode = .always
        let height = searchTextField.frame.height - 20
        let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: height + 10, height: height))
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: height, height: height)
        searchButton.layer.cornerRadius = 6
        searchButton.backgroundColor = UIColor(named: "blue") ?? .blue
        searchButton.setImage(UIImage(named: "search") ?? UIImage(), for: UIControl.State())
        searchButton.tintColor = .white
        searchButton.imageEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        rightPaddingView.addSubview(searchButton)
        
        searchTextField.rightView = rightPaddingView
        searchTextField.rightViewMode = .always
        searchTextField.becomeFirstResponder()
    }
}

