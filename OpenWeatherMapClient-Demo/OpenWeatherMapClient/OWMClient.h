//
//  OWMClient.h
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

typedef void(^networkResult)(Weather * _Nullable weather, NSError * _Nullable error);


@interface OWMClient : NSObject
+ (void)startWithToken:(NSString *_Nullable)token;
+ (void)getCityWeather:(NSString *_Nullable)city  completionHandler:(networkResult _Nullable )callBackBlock;
@end
