//
//  OWMClient.m
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import "OWMClient.h"
#import "NetworkDataFetcher.h"

@implementation OWMClient

static NSString * APIKey;
static NSString * baseURL;

+ (void)startWithToken:(NSString *_Nullable)token{
    APIKey = token;
    baseURL = @"https://api.openweathermap.org/data/2.5/weather";
}

+ (void)getCityWeather:(NSString *_Nullable)city  completionHandler:(networkResult _Nullable )callBackBlock{
    NetworkDataFetcher* networkDataFetcher = [[NetworkDataFetcher alloc] initWithUrl:baseURL andKey:APIKey];
    [networkDataFetcher getWeatherInfoForCity:city completion:^(Weather * _Nullable weather, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            callBackBlock(weather,error);
        });
    }];
}

@end
