//
//  NetworkDataFetcher.m
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import "NetworkDataFetcher.h"

@interface NetworkDataFetcher ()

@property (nonatomic) NSURLSession *session;
@property NSString *APIKey;
@property NSString *baseURL;

@end

@implementation NetworkDataFetcher

- (instancetype)initWithUrl:(NSString *)url andKey: (NSString *)key {
    self.baseURL = url;
    self.APIKey = key;
    
    self = [super init];
    if (self) {
        self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (void)getWeatherInfoForCity:(NSString *_Nullable)city  completion:(networkResult _Nullable )callBackBlock{
    NSString* url = [NSString stringWithFormat:@"%@?q=%@&appid=%@&units=metric", self.baseURL,city,self.APIKey];
    NSString* encodedUrl = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL* request = [[NSURL alloc] initWithString:encodedUrl];
    
    [[self.session dataTaskWithURL:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSDictionary *dictionaryResult = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingAllowFragments
                                                              error:nil];
            
            NSError* logicalError = [self checkErrorUsing:dictionaryResult];
            if (!logicalError) {
                Weather* weather = [[Weather alloc] initWithDict:dictionaryResult];
                callBackBlock(weather, nil);
            }else{
                callBackBlock(nil, logicalError);
            }
            return;
        }
        callBackBlock(nil, error);
    }] resume] ;
}

- (NSError* _Nullable)checkErrorUsing:(NSDictionary *)dictionary {
    int errorCode = [dictionary[@"cod"] intValue];
    if (!(errorCode < 300 && errorCode >= 200)) {
        NSString* errorDescription = dictionary[@"message"];
        NSError *error = [NSError errorWithDomain:@"OWMClient"
                                           code:errorCode
                                       userInfo:@{NSLocalizedDescriptionKey:errorDescription}];
        return error;
    }
    return nil;
}
@end
