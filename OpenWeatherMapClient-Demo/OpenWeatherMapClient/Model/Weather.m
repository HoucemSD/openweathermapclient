//
//  Weather.m
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import "Weather.h"

@implementation Weather

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        if (dict[@"name"] == nil) {
            return nil;
        }
        self.cityName = dict[@"name"];
        
        NSDictionary *mainDict = dict[@"main"];
        self.maxTemp = mainDict[@"temp_max"];
        self.minTemp = mainDict[@"temp_min"];
        self.currentTemp = mainDict[@"temp"];
        self.humidity = mainDict[@"humidity"];
        self.pressure = mainDict[@"pressure"];

        NSDictionary *weatherDict = [dict[@"weather"] lastObject];
        self.iconName = weatherDict[@"icon"];
        self.weatherDescription = weatherDict[@"description"];
        self.weatherTitle = weatherDict[@"main"];

    }
    return self;
}

@end
