//
//  Weather.h
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Weather : NSObject

@property (nonatomic, readwrite) NSString *_Nullable cityName;
@property (nonatomic, readwrite) NSNumber *_Nullable maxTemp;
@property (nonatomic, readwrite) NSNumber *_Nullable minTemp;
@property (nonatomic, readwrite) NSNumber *_Nullable currentTemp;
@property (nonatomic, readwrite) NSString *_Nullable iconName;
@property (nonatomic, readwrite) NSString *_Nullable weatherDescription;
@property (nonatomic, readwrite) NSString *_Nullable weatherTitle;
@property (nonatomic, readwrite) NSNumber *_Nullable humidity;
@property (nonatomic, readwrite) NSNumber *_Nullable pressure;


- (instancetype _Nullable )initWithDict:(NSDictionary *_Nullable)dict;
@end

