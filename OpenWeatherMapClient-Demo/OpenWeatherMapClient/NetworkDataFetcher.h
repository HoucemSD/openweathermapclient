//
//  NetworkDataFetcher.h
//  OpenWeatherMapClient-Demo
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

typedef void(^networkResult)(Weather * _Nullable weather, NSError * _Nullable error);

@interface NetworkDataFetcher : NSObject
- (instancetype _Nullable )initWithUrl:(NSString *_Nullable)url andKey: (NSString *_Nullable)key;
- (void)getWeatherInfoForCity:(NSString *_Nullable)city  completion:(networkResult _Nullable )callBackBlock;

@end
