//
//  OpenWeatherMapClient.h
//  OpenWeatherMapClient
//
//  Created by MacBook Pro on 11/08/2020.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for OpenWeatherMapClient.
FOUNDATION_EXPORT double OpenWeatherMapClientVersionNumber;

//! Project version string for OpenWeatherMapClient.
FOUNDATION_EXPORT const unsigned char OpenWeatherMapClientVersionString[];

#import <OpenWeatherMapClient/OWMClient.h>

